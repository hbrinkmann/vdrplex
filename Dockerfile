FROM alpine as builder
RUN apk add git
RUN mkdir /vdr
WORKDIR /vdr
RUN git clone https://github.com/forouher/VDR.bundle.git


FROM plexinc/pms-docker
USER root
WORKDIR "/config/Library/Application Support/Plex Media Server"
COPY --from=builder /vdr/VDR.bundle "Plug-ins/VDR.bundle"
COPY --from=builder /vdr/VDR.bundle/Scanners Scanners
